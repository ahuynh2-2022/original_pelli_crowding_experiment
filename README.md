# original_Pelli_Crowding_Experiment

Original study conducted by Pelli on Foveal Crowding

==Instructions==

Open toolbox/CriticalSpacing-master/runAPLabScript.m to run experiment
* Always update your name and name of subject
* Log all changes using git.

 = Currently Working On =

 == Luke ==

 == Aaron ==
 1. Sifting through Crowded and Uncrowded scripts to find differences before merging into one (1) scripts
 2.  Merge Crowded and Uncrowded conditions into one scripts
 3.  Add dynamic and fixation and feedback sound referenced from Ying Lin (Duje Tadin Lab)

